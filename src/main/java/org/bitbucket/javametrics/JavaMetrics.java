package org.bitbucket.javametrics;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.*;
import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.stmt.*;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.github.javaparser.printer.lexicalpreservation.LexicalPreservingPrinter;
import com.opencsv.CSVWriter;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * This class allows to measure metrics of a given java file.
 */
public class JavaMetrics {
    //path for the given file
    private String pathName;
    //the compiled unit is the entire AST resulting from parsing and represents its root
    private CompilationUnit cu;
    private String primaryType; //type with the same name as the file

    //lists to keep reports of classes and methods in the file
    List<ClassReportEntry> classReports;
    List<MethodReportEntry> methodReports;

    /**
     * Initialization
     * @param file java file to parse and measure metrics of
     * @throws FileNotFoundException
     */
    public JavaMetrics(File file) throws FileNotFoundException{
        this.pathName = file.getPath();
        this.cu = StaticJavaParser.parse(file);

        methodReports = new ArrayList<MethodReportEntry>();
        classReports = new ArrayList<ClassReportEntry>();

        if (cu.getPrimaryType().isPresent()) {
            this.primaryType = cu.getPrimaryTypeName().get();
        }

    }

    /**
     * ClassReportEntry keeps relevant information collected on each encountered class
     */
    private static class ClassReportEntry{
        private String path;
        private String name;
        private int loc;
        private int cloc;
        private float dc;
        private int wmc;
        private float bc;

        static String[] getHeader(){
            String[] header = { "chemin", "class", "classe_LOC",
                                "classe_CLOC", "classe_DC", "WMC", "classe_BC" };
            return header;
        }

        /**
         * constructor
         * @param path
         * @param name
         * @param loc
         * @param cloc
         * @param dc
         */
        ClassReportEntry(String path, String name, int loc, int cloc, float dc){
            this.path = path;
            this.name = name;
            this.loc = loc;
            this.cloc = cloc;
            this.dc = dc;
            this.wmc = 0; //initialized at 1 to avoid div by zero
            this.bc = 0;
        }


        private String[] toStrArr(){
            String[] data = {this.path, this.name, Integer.toString(this.loc), Integer.toString(this.cloc),
                    Float.toString(dc), Integer.toString(this.wmc), Float.toString(bc)};
            return data;
        }


    }

    /**
     * MethodReportEntry keeps relevant information collected on each encountered method
     */
    private static class MethodReportEntry{
        private String path;
        private String parentClass;
        private String name;
        private int loc;
        private int cloc;
        private float dc;
        private int cc; //cyclomatic complexity
        private float bc;

        //useful header for making csv files with data in entries
        static String[] getHeader() {
            String[] header =  { "chemin", "class", "methode",
                                "methode_LOC", "methode_CLOC",
                                "methode_DC", "CC", "methode_BC" };
            return header;
        }

        MethodReportEntry(String path, String parentClass, String name, int loc, int cloc, float dc, int cc, float bc){
            this.path = path;
            this.parentClass = parentClass;
            this.name = name;
            this.loc = loc;
            this.cloc = cloc;
            this.dc = dc;
            this.cc = cc;
            this.bc = bc;
        }

        //string array format of the data for writing rows in a csv file
        private String[] toStrArr(){
            String[] data = {this.path, this.parentClass, this.name, Integer.toString(this.loc), Integer.toString(this.cloc),
                    Float.toString(dc), Integer.toString(cc), Float.toString(bc)};
            return data;
        }
    }


    /**
     * get classes, interfaces and enums from the compilation unit
     * @return List of classes, interfaces, and enums found
     */
    private List<TypeDeclaration<?>> getClasses(){
        List<TypeDeclaration<?>> classes = new ArrayList<>();

        VoidVisitorAdapter<List<TypeDeclaration<?>>> classCollector = new ClassCollector();
        classCollector.visit(this.cu, classes);
        VoidVisitorAdapter<List<TypeDeclaration<?>>> enumCollector = new EnumCollector();
        enumCollector.visit(this.cu, classes);

        return classes;
    }


    /**
     * visits the AST and collects nodes of class or interface declarations
     */
    private class ClassCollector extends VoidVisitorAdapter<List<TypeDeclaration<?>>> {
        @Override
        public void visit(ClassOrInterfaceDeclaration c, List<TypeDeclaration<?>> collector){
            super.visit(c, collector);
            collector.add(c);
        }
    }

    /**
     * visits the AST and collects nodes of Enum declaration
     */
    private class EnumCollector extends VoidVisitorAdapter<List<TypeDeclaration<?>>> {
        @Override
        public void visit(EnumDeclaration e, List<TypeDeclaration<?>> collector){
            super.visit(e, collector);
            collector.add(e);
        }
    }

    /**
     * Get nodes of methods within a class in the AST.
     * Only the immediate children of the class node are considered;
     * the methods of an inner class belong to the innerclass.
     * @param c class, instance, enum
     * @return a list of methods
     */
    private List<MethodDeclaration> getMethods(TypeDeclaration c) {
        List<MethodDeclaration> methods = new ArrayList<>();
        for (Node n : c.getChildNodes()){
            if (n instanceof MethodDeclaration) {
                methods.add((MethodDeclaration) n);
            }
        }
        return methods;
    }

    /**
     * Counts the number of statements such as 'if' 'case' 'while'
     * to yield the cyclomatic complexity of a method
     * @param m a method node
     * @return number representing complexity of the code in method declaration
     */
    private int getCyclomaticComplexity(MethodDeclaration m){
        int complexity = 1;
        for (Node n : m.findAll(Node.class)) {
            if (n instanceof SwitchEntry || n instanceof IfStmt
                    || n instanceof DoStmt || n instanceof ForStmt
                    || n instanceof ForEachStmt || n instanceof WhileStmt) {
                complexity += 1;
            }
        }
        return complexity;
    }

    /**
     * Given a node from the AST, counts LOC of its source code
     * @param node Node in the AST
     * @return number of non-blank lines in the node
     * @throws IOException
     */
    static private int getLOC(Node node) throws IOException {
        //preserve original formatting of source code
        LexicalPreservingPrinter.setup(node);
        return countLOC(LexicalPreservingPrinter.print(node));
    }

    /**
     * counts the non-blank lines in code.
     * @param src String representing a snippet of code
     * @return number of non-blank lines
     * @throws IOException
     */
    static private int countLOC(String src) throws IOException {
        int count = 0;
        BufferedReader br = new BufferedReader(new StringReader(src));
        String line;
        while ((line = br.readLine()) != null) {
            if(!line.trim().isEmpty()){
                count++;
            }
        }
        return count;
    }

    /**
     * Given a node from the AST, counts the lines of comment its code contains
     * @param node Node in the AST
     * @return number of lines of code containing comments
     */
    static private int getCLOC(Node node){
        int count = 0;
        //get all the comments inside the node
        List<Comment> comments = node.getAllContainedComments();
        if (node.getComment().isPresent()){
            comments.add(node.getComment().get());
        }

        //add up the number of lines of each comment
        for (Comment c : comments) {
            //count += countLOC(c.getContent());
            count += (c.getRange().get().end.line - c.getRange().get().begin.line + 1);
        }
        return count;
    }

    /**
     * calculates the comment density of a code segment given their LOC
     * and CLOC.
     * @param cloc comment lines of code
     * @param loc lines of code
     * @return comment density representing the ratio
     */
    private float commentDensity(int cloc, int loc) {
        return cloc/(float) loc;
    }

    /**
     * Measure all metrics of the file input to this JavaMetrics instance
     * @throws IOException
     */
    void measureAllMetrics() throws IOException {

        List<TypeDeclaration<?>> classes = getClasses();

        //measure all the
        for (TypeDeclaration<?> c : classes) {
            measureClassMetrics(c);
            measureMetricsOfMethods(c);
        }
        //calculate WMC and method bc
        for (MethodReportEntry mre : methodReports){
            findClassReportEntry(mre.parentClass).wmc += mre.cc;
            mre.bc = mre.dc / mre.cc;
        }
        for (ClassReportEntry cre : classReports) {
            if (cre.wmc == 0){ //i.e, class has no method
                cre.wmc = 1; //adjustment to avoid division by zero
            }
            cre.bc = cre.dc/cre.wmc;
        }
    }


    //measure metrics for one class
    void measureClassMetrics(TypeDeclaration<?> c) throws IOException  {
        int loc = getLOC(c);
        int cloc = getCLOC(c);
        String name = c.getNameAsString();

        // include LOC and CLOC of import and package declaration types
        // into the primary type's measures.
        if (primaryType != null && name.equals(primaryType)){
            for (ImportDeclaration i : cu.getImports()){
                loc += getLOC(i);
                cloc += getCLOC(i);
            }
            if (cu.getPackageDeclaration().isPresent()){
                PackageDeclaration p = cu.getPackageDeclaration().get();
                loc += getLOC(p);
                cloc += getCLOC(p);
            }
        }
        classReports.add(new ClassReportEntry(this.pathName, c.getNameAsString(),
                loc, cloc, commentDensity(cloc, loc)));

    }



    //measure the metrics of methods in the specified class
    void measureMetricsOfMethods(TypeDeclaration<?> c) throws IOException {
        String className = c.getNameAsString();
        //measure metric of each class
        for (MethodDeclaration m : getMethods(c)) {
            int mLoc = this.getLOC(m); //method LOC
            int mCloc = this.getCLOC(m); //method lines of comments

            float cd = commentDensity(mCloc, mLoc);
            int cc = getCyclomaticComplexity(m);
            //add method to the method report
            methodReports.add(new MethodReportEntry(this.pathName, className,
                                        this.getNameWithParam(m), mLoc, mCloc,
                                        cd, cc, cd/cc));
        }
    }

    private ClassReportEntry findClassReportEntry(String className){
        for (ClassReportEntry cre : classReports) {
            if(cre.name.equals(className)){
                return cre;
            }
        }
        return null;
    }



    String getNameWithParam(MethodDeclaration md) {
        String name = md.getNameAsString();
        for (Parameter p : md.getParameters()){
            name += "_"+ p.getTypeAsString();
        }
        return name;
    }
    /**
     * write class metrics into csv file
      * @param file specified csv file
     */
    void writeClassReport(File file){
        try {
            CSVWriter writer = new CSVWriter(new FileWriter(file, true),
                    CSVWriter.DEFAULT_SEPARATOR,CSVWriter.NO_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);

            // write header column if the file is fresh
            if (file.length() == 0){
                String[] header = ClassReportEntry.getHeader();
                writer.writeNext(header);
            }

            // Values
            for (ClassReportEntry cre : classReports){
                writer.writeNext(cre.toStrArr());
            }
            writer.close();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();

        }

    }

    /**
     * write method metrics into csv file
     * @param file specified csv file
     */
    void writeMethodReport(File file){
        try {
            CSVWriter writer = new CSVWriter(new FileWriter(file,true),
                    CSVWriter.DEFAULT_SEPARATOR,CSVWriter.NO_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);

            //write header if the file is new.
            if (file.length() == 0) {
                String[] header = MethodReportEntry.getHeader();
                writer.writeNext(header);
            }

            // Values
            for (MethodReportEntry mre : methodReports){
                writer.writeNext(mre.toStrArr());
            }
            writer.close();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();

        }
    }


}
