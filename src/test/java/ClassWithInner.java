class ClassWithInner {
    int x;
    void outerClassMethod(int arg1) {
        x = arg1;
    }

    class InnerClass {
        int y;
        void innerClassMethod1(int arg1){
            y = arg1;
        }
        void innerClassMethod2(int arg2){
            y = arg2;
        }
    }
}

class MyMainClass {
    public static void main(String[] args) {
        ClassWithInner myOuter = new ClassWithInner();
        ClassWithInner.InnerClass myInner = myOuter.new InnerClass();
        System.out.println(myInner.y + myOuter.x);
    }
}
