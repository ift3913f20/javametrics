package org.bitbucket.javametrics;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.io.File;
import java.nio.file.Path;

/**
 * Unit test for simple App.
 */
public class AppTest 
{

    String innerClass = "class OuterClass {\n"
            +  "int x;\n"
            +  "void outerClassMethod(arg1) {\n"
            + 	"x = arg1;\n"
            + "}\n\n"
            + "class InnerClass {\n"
            +   "int y;\n"
            +   "void innerClassMethod1(arg1){\n"
            +    	"y = arg1;\n"
            +    "}\n"
            +    "void innerClassMethod2(arg2){\n"
            +    	"\t\t\ty = arg2;\n"
            +    "\t\t}\n"
            +  "\t}\n"
            +"}\n\n"
            + "public class MyMainClass {\n"
            +  "public static void main(String[] args) {\n"
            +    "OuterClass myOuter = new OuterClass();\n"
            +    "OuterClass.InnerClass myInner = myOuter.new InnerClass();\n"
            +    "System.out.println(myInner.y + myOuter.x);\n"
            +  "}\n}\n";

    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
       try{
            JavaMetrics test = new JavaMetrics(new File("~/IdeaProjects/javametrics/src/test/resources/classWithInner.java"));
        }
        catch (Exception e) {
            System.err.println(e.getMessage());

        }
        assertTrue(true);
    }
}
