package org.bitbucket.javametrics;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class App {

    public static void main( String[] args ) {

        String currentDirectory = System.getProperty("user.dir");

        try {
            //path names for output files
            File classesCSV = new File(currentDirectory + "/classes.csv");
            File methodsCSV = new File(currentDirectory + "/methodes.csv");
            //wipe output files
            new PrintWriter(classesCSV).close();
            new PrintWriter(methodsCSV).close();


            File dir = new File(args[0]); //arg[0] = project directory passed as argument
            //extract all .java files from given directory
            List<File> javaFiles = new LinkedList<File>();
            getJavaFiles(dir, javaFiles);

            //for each of the .java file found, invoke javametrics to make a report
            // and write it out on output files.
            if (javaFiles.size() > 0) {
                for (File jf : javaFiles) {
                    JavaMetrics jm = new JavaMetrics(jf);
                    jm.measureAllMetrics();
                    jm.writeClassReport(classesCSV);
                    jm.writeMethodReport(methodsCSV);
                }

            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }


    /**
     * Recursively get all files with the extension .java in a project directory
     * @param dir project directory
     * @param fileList accumulator for files. Normally an empty list when method is called.
     */
    static void getJavaFiles(File dir, List<File> fileList) {

        if (dir.listFiles() == null) return;

        for (File f : dir.listFiles()) {
            if (f.isFile() && f.getName().endsWith(".java")) {
                fileList.add(f);
            } else if (dir.isDirectory()) { //if f is a directory
                getJavaFiles(f, fileList);
            }
        }


    }


}




